const colors = {
  color: "#2c3e50",
  border: "#bdc3c7",
  headerBackground: "#ecf0f1",
  light: "#bdc3c7",
  dark: "#2c3e50",
};

export default colors;
