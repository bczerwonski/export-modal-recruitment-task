import styled from "styled-components";
import { Formik, Form } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import colors from "./colors";

import Button from "./components/Button";
import { Input, Select } from "./components/Input";

function App() {
  const notify = () => toast("🎉 Success!");

  return (
    <>
      <ToastContainer />
      <Formik
        initialValues={{
          reportName: "",
          format: "Excel",
          email: "",
          schedule: { type: "No Reapeat" },
        }}
        onSubmit={async (values) => {
          await fetch("https://postman-echo.com/post", {
            method: "POST",
            mode: "no-cors",
            body: JSON.stringify(values),
          });
          notify();
        }}
      >
        {({ values, handleSubmit, handleChange, handleBlur, isSubmitting }) => (
          <Modal onSubmit={handleSubmit}>
            <Header>Export Report</Header>
            <Content>
              <label htmlFor="reportName">Report name</label>
              <Input
                type="text"
                id="reportName"
                name="reportName"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.reportName}
                placeholder="Shareablee Report"
              />

              <label htmlFor="format">Format</label>
              <Group>
                <Group>
                  <input
                    type="radio"
                    id="Excel"
                    name="format"
                    value="Excel"
                    checked={values.format === "Excel"}
                    onChange={handleChange}
                  />
                  <label htmlFor="Excel">Excel</label>
                </Group>
                <Group>
                  <input
                    type="radio"
                    id="CSV"
                    name="format"
                    value="CSV"
                    checked={values.format === "CSV"}
                    onChange={handleChange}
                  />
                  <label htmlFor="CSV">CSV</label>
                </Group>
              </Group>
              <label htmlFor="email">E-mail to</label>
              <Input
                type="email"
                id="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                placeholder="client@company.com"
              />

              <label htmlFor="shedule">Schedule</label>
              <Group>
                <Group>
                  <input
                    type="radio"
                    id="No Reapeat"
                    name="schedule.type"
                    value="No Reapeat"
                    checked={values.schedule.type === "No Reapeat"}
                    onChange={handleChange}
                  />
                  <label htmlFor="No Reapeat">No Reapeat</label>
                </Group>
                <Group>
                  <input
                    type="radio"
                    id="Specific Date"
                    name="schedule.type"
                    value="Specific Date"
                    checked={values.schedule.type === "Specific Date"}
                    onChange={handleChange}
                  />
                  <label htmlFor="Specific Date">Specific Date</label>
                </Group>
                <Group>
                  <input
                    type="radio"
                    id="Daily"
                    name="schedule.type"
                    value="Daily"
                    checked={values.schedule.type === "Daily"}
                    onChange={handleChange}
                  />
                  <label htmlFor="Daily">Daily</label>
                </Group>
                <Group>
                  <input
                    type="radio"
                    id="Weekly"
                    name="schedule.type"
                    value="Weekly"
                    checked={values.schedule.type === "Weekly"}
                    onChange={handleChange}
                  />
                  <label htmlFor="Weekly">Weekly</label>
                </Group>
              </Group>
              {values.schedule.type === "No Reapeat" && <br />}
              {values.schedule.type === "Specific Date" && (
                <>
                  <label>Date</label>
                  <Group>
                    <Input
                      type="date"
                      name="schedule.date"
                      value={values.schedule.date}
                      onChange={handleChange}
                    />
                    <span>at</span>
                    <Input
                      type="time"
                      name="schedule.time"
                      value={values.schedule.time}
                      onChange={handleChange}
                    />
                  </Group>
                </>
              )}
              {values.schedule.type === "Daily" && (
                <>
                  <label htmlFor="time">Everyday at</label>
                  <Group>
                    <Input
                      type="time"
                      id="time"
                      name="schedule.time"
                      value={values.schedule.time}
                      onChange={handleChange}
                    />
                  </Group>
                </>
              )}
              {values.schedule.type === "Weekly" && (
                <>
                  <label htmlFor="day">Every</label>
                  <Group>
                    <Select
                      name="schedule.day"
                      id="day"
                      value={values.schedule.day}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    >
                      <option value="" label="Select day" />
                      <option value="Monday" label="Monday" />
                      <option value="Tuesday" label="Tuesday" />
                      <option value="Wednesday" label="Wednesday" />
                      <option value="Thursday" label="Thursday" />
                      <option value="Friday" label="Friday" />
                      <option value="Saturday" label="Saturday" />
                      <option value="Sunday" label="Sunday" />
                    </Select>
                  </Group>
                </>
              )}
            </Content>
            <Footer>
              <Button light disabled={isSubmitting} onClick={notify}>
                Cancel
              </Button>
              <Button disabled={isSubmitting} type="submit">
                OK
              </Button>
            </Footer>
          </Modal>
        )}
      </Formik>
    </>
  );
}

export default App;

const Modal = styled(Form)`
  border: 1px solid ${colors.border};

  max-width: 512px;
  width: 100%;

  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
`;

const Header = styled.div`
  background-color: ${colors.headerBackground};
  padding: 16px;
`;

const Content = styled.div`
  border-bottom: 2px solid ${colors.border};
  display: grid;
  grid-template-columns: auto 1fr;
  gap: 24px 32px;
  align-items: center;
  padding: 16px;
`;

const Footer = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 16px;
`;

const Group = styled.div`
  display: inline-flex;
  gap: 4px;
  align-items: center;
`;
