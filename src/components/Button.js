import styled from "styled-components";

import colors from "../colors";

const Button = styled.button`
  outline: none;
  cursor: pointer;

  padding: 8px;
  margin: 8px;

  min-width: 92px;

  color: ${({ light }) => (light ? colors.border : `white`)};

  background-color: ${({ light }) => (light ? `white` : colors.dark)};

  border: solid 1px ${({ light }) => (light ? colors.border : colors.dark)};

  border-radius: 0;

  &:hover {
    background-color: ${({ light }) => (light ? colors.dark : colors.light)};
  }

  transition: background-color 0.1s;

  &:disabled {
    background-color: ${colors.light};
    color: white;
  }
`;

export default Button;
