import styled from "styled-components";

import colors from "../colors";

const Input = styled.input`
  padding: 8px;

  border: 1px solid ${colors.border};
`;

const Select = styled.select`
  padding: 8px;

  border: 1px solid ${colors.border};
`;

export { Input, Select };
